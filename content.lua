require("xmlreader")

function split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

ngx.log(ngx.DEBUG, "Body Filter BEGIN")
data = ngx.req.get_body_data()
if (data) then
    local r = assert(xmlreader.from_string(data))
    while (r:read()) do
        if (r:node_type() == 'element') then
            if (r:name() == 'soap:Body') then
                inBody = true
            elseif (inBody) then
                -- First element after body is our message header
                local soap_action = r:name()
                local parsed = split(soap_action, "_")
                local soap_action1 = parsed[1] or '-'
                local soap_action2 = parsed[2] or '-'
                local soap_action3 = parsed[3] or '-'
                ngx.var.soap_action = soap_action
                ngx.var.soap_action1 = soap_action1
                ngx.var.soap_action2 = soap_action2
                ngx.var.soap_action3 = soap_action3
                break
            end
        end
    end
end
ngx.log(ngx.DEBUG, "Body Filter END")
