FROM nginx:latest

RUN apt-get update && apt-get install -y nginx-extras luarocks libxml2-dev && apt-get clean
RUN luarocks install lua-xmlreader


COPY templates/default.conf.template /etc/nginx/templates/
COPY templates/10-lua-logging.conf.template /etc/nginx/templates/

COPY init.lua /usr/share/nginx/init.lua
COPY content.lua /usr/share/nginx/content.lua
COPY logger.lua /usr/share/nginx/logger.lua
