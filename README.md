# nginx request body logging

In this project there are experiments to log request body content for POST calls using nginx.

## Option 1 - full request body through standard nginx logging

The standard nginx request logging, which allows logging the full request body to the log file without being transformed, resulting in a multiline log. This can be done by changing the nginx logging format to include the request body.

The log entry in this case will look like the one below:

```text
172.17.0.1 - - [29/Apr/2022:07:17:49 +0000] "POST / HTTP/1.1" 200 16 "-" "curl/7.74.0" "0.000" "0.000" "0.001" "@@<?xml version=\x221.0\x22 encoding=\x22utf-8\x22?><soap:Envelope xmlns:soap=\x22http://schemas.xmlsoap.org/soap/envelope/\x22>  <soap:Body>    <SystemService_ConverterModule_NumberToWords xmlns=\x22http://www.dataaccess.com/webservicesserver/\x22>      <ubiNum>500</ubiNum>    </SystemService_ConverterModule_NumberToWords>  </soap:Body></soap:Envelope>@@"
```

The advantages of this approach are that it requires just a standard nginx installation or container and are simple to implement as they are just standard nginx configuration.

The disadvantages of this approach are that it shifts the burden of dealing with the multiline log to the log aggregator and potentially drops confidential information from the request into the log file or log aggregator.

## Option 2 - selective request body through Lua scripting

Nginx supports scripting with Lua in different stages of the request workflow. In specific phases of the request processing workflow, the Lua scripts have access to the request body.

![Request processing workflow](https://cloud.githubusercontent.com/assets/2137369/15272097/77d1c09e-1a37-11e6-97ef-d9767035fc3e.png)

The example implementation logs the name of the SOAP request message and parses it in up to 3 stages, emitting them all into the log registry as below:

```text
172.17.0.1 - - [29/Apr/2022:07:17:21 +0000] "POST / HTTP/1.1" 200 16 "-" "curl/7.74.0" "0.000" "0.000" "SystemService_ConverterModule_NumberToWords" "SystemService" "ConverterModule" "NumberToWords"
```

The advantages of this approach are that it allows tighter control of what gets added to the log file, allowing to use Lua modules to potentially parse the request payload and issue logs containing only the minimal information expected.

The disadvantages of this approach is that it requires a nginx installation/container that supports Lua scripting (either a rebuilt version of the standard nginx container or openresty), it requires changes to the configuration to support Lua directives which might not be familiar to all operators, and finally it requires the Lua code to be tested as any other code.

## References

- [How To Use the Official NGINX Docker Image](https://www.docker.com/blog/how-to-use-the-official-nginx-docker-image/)
- [nginx official Dockerfile](https://github.com/nginxinc/docker-nginx/blob/master/Dockerfile-debian.template)
- [Request Capturing using NGINX and Lua](https://tarunlalwani.com/post/request-capturing-nginx-lua/)
- [Lua Ngx API - read_body](https://openresty-reference.readthedocs.io/en/latest/Lua_Nginx_API/#ngxreqread_body)
- [lua-nginx-module: lua_need_request_body](https://github.com/openresty/lua-nginx-module#lua_need_request_body)
- [Logging POST data from $request_body](https://stackoverflow.com/questions/4939382/logging-post-data-from-request-body)
- [Everything You Need to Know about NGINX Logs](https://adamtheautomator.com/nginix-logs/)
- [Module ngx_http_core_module: var $request_body](https://nginx.org/en/docs/http/ngx_http_core_module.html#var_request_body)
- [Full request/response body in nginx](https://gist.github.com/morhekil/1ff0e902ed4de2adcb7a)
- [Full request/response body in nginx - fork](https://gist.github.com/shoeb751/84be11193ac1f3514c5eb4d70a8ba973)
- [Lua Ngx API - get_body_data](https://openresty-reference.readthedocs.io/en/latest/Lua_Nginx_API/#ngxreqget_body_data)
- [How to parse a XML file in Lua?](https://stackoverflow.com/questions/40634946/how-to-parse-xml-file-in-lua)
- [nginx proxy cache – log the upstream response server, time, cache status, connect time and more in nginx access logs](https://ahelpme.com/software/nginx/nginx-proxy-cache-log-the-upstream-response-server-time-cache-status-connect-time-and-more-in-nginx-access-logs/)
- [busted project repository](https://github.com/lunarmodules/busted)
- [Testing nginx functionality with Lua unit tests](http://blog.travel.cloud/engineering-blog/testing-nginx-functionality-with-lua-unit-tests)
- [Testing lua script with busted](https://stackoverflow.com/questions/23377991/testing-lua-script-with-busted)
- [How to mock a global function in Lua](https://github.com/lunarmodules/busted/issues/400)
- [The Journeyman's guide to Lua Unit Testing](https://blog.insiderattack.net/the-journeymans-guide-to-lua-unit-testing-41642825314a)
- [GitLab CI: Services](https://datawookie.dev/blog/2020/12/gitlab-ci-services/)
- [How to build docker images in a Gitlab CI pipeline](https://www.howtogeek.com/devops/how-to-build-docker-images-in-a-gitlab-ci-pipeline/)
- [LuaLanes and LuaSockets](https://stackoverflow.com/questions/13219570/lualanes-and-luasockets)
