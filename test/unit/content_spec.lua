function readAll(file)
    local f = assert(io.open(file, "rb"))
    local content = f:read("*all")
    f:close()
    return content
end

local function script_path()
    local str = debug.getinfo(2, "S").source:sub(2)
    return str:match("(.*[/\\])") or "./"
end

describe('payload processing', function ()
    it('should log SOAP actions for correct message', function ()
        ngx = mock({
            log = function() end, 
            DEBUG = 0,
            req = {
                get_body_data = function ()
                   return readAll(script_path() .. "/message.xml") 
                end
            },
            var = {}
        })
        _G.ngx = ngx

        require('../content')

        assert.spy(ngx.log).called_with(ngx.DEBUG, 'Body Filter BEGIN')

        assert.truthy(ngx.var.soap_action)
        assert.equals(ngx.var.soap_action, 'SystemService_ConverterModule_NumberToWords')

        assert.truthy(ngx.var.soap_action1)
        assert.equals(ngx.var.soap_action1, 'SystemService')

        assert.truthy(ngx.var.soap_action2)
        assert.equals(ngx.var.soap_action2, 'ConverterModule')

        assert.truthy(ngx.var.soap_action3)
        assert.equals(ngx.var.soap_action3, 'NumberToWords')

        assert.spy(ngx.log).called_with(ngx.DEBUG, 'Body Filter END')
    end)
end)
