redis = require('redis')
host = os.getenv('REDIS_HOST', 'localhost')

describe('exercise redis', function ()
    it('should connect to redis', function ()
        local client = redis.connect(host)
        local response = client:ping()
        assert.truthy(client)
        assert.truthy(response)
    end)

    it('should save values to redis', function ()
        local client = redis.connect(host)
        client:set('saved value', 5)
        local value = client:get('saved value')
        -- Always returns strings
        assert.same('5', value, 'Incorrect value retrieved from redis')
    end)

end)
