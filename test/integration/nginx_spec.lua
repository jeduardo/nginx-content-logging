-- luarocks --lua-version 5.1 --local install luasocket
-- luarocks --lua-version 5.1 --local install penlight
-- luarocks --lua-version 5.1 --local install lua-json
-- luarocks --lua-version 5.1 --local install lanes
-- luarocks --lua-version 5.1 --local install busted
-- luarocks --lua-version 5.1 --local install luaposix

local lanes = require('lanes')
lanes.configure()
local linda = lanes.linda()
local stringx = require('pl.stringx')
local json = require('json')
local posix = require('posix')
local redis = require('redis')
local inspect = require('inspect')

local redis_params = {
    host = os.getenv('REDIS_HOST') or '127.0.0.1',
    port = os.getenv('REDIS_PORT') or 6379,
    channel = os.getenv('SYSLOG_KEY') or 'syslog'
}
print(inspect(redis_params))

local nginx_params = {
    host = os.getenv('NGINX_HOST') or '127.0.0.1',
    port = os.getenv('NGINX_PORT') or '8080'
}
print(inspect(nginx_params))

local function script_path()
    local str = debug.getinfo(2, "S").source:sub(2)
    return str:match("(.*[/\\])") or "./"
end

local function readAll(file)
    local f = assert(io.open(file, "rb"))
    local content = f:read("*all")
    f:close()
    return content
end

local function server(params)
    --print('About to load listener dependencies')

    local stringx = require('pl.stringx')
    local socket = require('socket')
   
    --print('Server dependencies loaded')

    --print('Connecting to redis')

    local client = redis.connect(redis_params)
    client:select(15)

    print('Listening for messages')

    for msg, abort in client:pubsub({ subscribe = redis_params.channel }) do
        if msg.kind == 'message' then
            local data = stringx.strip(msg.payload)
            linda:send("payload", data)
        end
    end
end

local function nextPayload()
    local key, val = linda:receive(10.0, "payload")
    if val == nil then
        print("timeout")
        return nil
    end
    local parsed = stringx.split(val, ' nginx: ')
    --print('Parsed data', parsed)
    local payload = json.decode(parsed[2])
    --print('Processed payload:', payload)
    return payload
end

-- passing all libraries (*) to new lua context
local syslog = lanes.gen("*", server)(redis_params, linda)
--print(syslog)

-- leaving some time for the fake syslog service to come alive
posix.sleep(1)

describe('when nginx receives a SOAP post', function ()
    it('should log SOAP action fields', function ()
        -- POST message.xml to nginx
        local reqbody = readAll(script_path() .. "/message.xml")
        local respbody = {}
        local http = require("socket.http")
        local ltn12 = require("ltn12")
        http.request({
                method = "POST",
                url = 'http://' .. nginx_params.host .. ':' .. nginx_params.port .. '/',
                source = ltn12.source.string(reqbody),
                headers = {
                    ["content-type"] = "application/xml",
                    ["Host"] = "lua-logging",
                    ["content-length"] = tostring(#reqbody)
                },
                sink = ltn12.sink.table(respbody)
            })
        -- Check whether the right log was emitted
        local payload = nextPayload()
        assert.truthy(payload, 'Payload is empty')
        assert.equals('SystemService_ConverterModule_NumberToWords', payload.soap_action)
        assert.equals('SystemService', payload.soap_action1)
        assert.equals('ConverterModule', payload.soap_action2)
        assert.equals('NumberToWords', payload.soap_action3)
    end)
end)
