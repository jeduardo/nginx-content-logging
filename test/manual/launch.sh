#!/bin/bash

IMG=$2
OPTION=$1
IMG_NAME=$3

# Config validation
if [ "x$OPTION" == "x" ]; then
  OPTION="standard"
fi

if [ "x$IMG" == "x" ]; then
  IMG="jeduardo/nginx"
fi

if [ "x$IMG_NAME" == "x" ]; then
  IMG_NAME="nginx-logging"
fi

# Env prep
mkdir tmp || true
(docker stop $IMG_NAME && docker rm $IMG_NAME) || true

# Execution
docker run \
  -p 8080:80 \
  --name $IMG_NAME \
   --cap-add=sys_nice \
  -v /etc/passwd:/etc/passwd:ro \
  -v /etc/group:/etc/group:ro \
  -v "${PWD}/nginx.${OPTION}.conf:/etc/nginx/nginx.conf:ro" \
  -v "${PWD}/init.lua:/usr/share/nginx/init.lua:ro" \
  -v "${PWD}/content.lua:/usr/share/nginx/content.lua:ro" \
  -v "${PWD}/logger.lua:/usr/share/nginx/logger.lua:ro" \
  -v "${PWD}/tmp:/tmp:rw" \
  -v "${PWD}:/tmp/files:rw" \
  $IMG
