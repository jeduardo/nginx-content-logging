#!/bin/bash

curl -v \
  -X POST \
  -H "Content-type: application/xml" \
  -H "Host: lua-logging" \
  --data @message.xml \
  http://localhost:8080/
